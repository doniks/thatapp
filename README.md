[![pipeline status](https://gitlab.com/doniks/thatapp/badges/master/pipeline.svg)](https://gitlab.com/doniks/thatapp/commits/master)
# that app 

A gitlab example for using a c library in an **optional** submodule

## How to work on the app
 * First clone the source code for **thatapp** and the "default" submodules:
    ```
    git clone https://gitlab.com/doniks/thatapp.git
    cd thatapp
    ./git-submodules-default.sh
    ```
 * Now you can edit the app source code, i.e., `main.c`, etc
 * To build it run `make`
 * To test it you can run `make run`
 * In order to propose your contribution upstream, do your normal
    ```
    git remote add YOUR_FORK
    git checkout -b YOUR_FEATURE_BRANCH
    git commit -m MESSAGE
    git push --set-upstream YOUR_FORK YOUR_FEATURE_BRANCH
    ```
    And then create a Merge Request on gitlab

## How to work on the BigLib

**BigLib** is a big software project independent of **thatapp**. Most developers
will never touch the source code of **BigLib**. You find the public header and
prebuilt library in
```
  libs/biglib.h
  libs/biglib.a
```
and that is all you need to work on **thatapp**.

However, if you want to work on **BigLib** directly you can clone this
"optional" submodule right here and include it in your edit/build/test
cycle:

  * Get the source
    ```
    git-submodule-all.sh
    ```
  * Now you can edit the source code of **BigLib**, i.e., `libs/biglib/biglib.c`, etc
  * You can build and test the library itself:
    ```
    cd libs/biglib
    make run
    ```
  * To update `libs/biglib.h` and `libs/biglib.a` and use it to build and test the app, run these commands from the root directory of **thatapp**:
    ```
    make biglib
    make
    make run
    ```
  * In order to propose your contribution upstream, you probably need to make *synchronized* merge requests to both projects. Let's describe a few typical examples
    * A contribution to **BigLib**, without a strict relation to changes in **thatapp**:
      1) Make a **BigLib** MR. Get it reviewed and merged.
      2) (Optional, but typical) *after* the BigLib change is merged:
        1) update your local working copy `git checkout master; git pull`
        2) rebuild and test `cd ../.. ; make biglib; make; make run`
        3) create a MR for **thatapp** to update
        * the `lib/biglib` submodule
        * the prebuilt library `lib/biglib.a` and 
        * the header `lib/biglib.h`
    * A new feature in **BigLib** with specific, related changes in **thatapp**:
      1) Make a **BigLib** MR, and *also*
      1) make a **thatlib** WIP MR, showing how this feature is to be integrated. Link the MRs. Preferably allow **thatlib** maintainers to commit to your WIP MR branch.
      1) *After* the **BigLib** change is merged, update your MR to update submodule, library and header. Alternatively, the **thatapp** maintainer might do this directly to avoid delays.
