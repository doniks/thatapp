OBJ:=main.o
LIBS:=libs/thatlib.a libs/biglib.a
CFLAGS+=-Werror

app: ${OBJ} ${LIB}
	gcc -o $@  ${OBJ} ${LIBS}

main.o: main.c
	gcc ${CFLAGS} -I libs -c  main.c

run: app
	./app

clean:
	rm -rf *.o app

veryclean:clean
	make -C libs/thatlibrary clean
	make -C libs/biglib clean
	rm -rf libs/*.a

force: veryclean thatlib biglib

thatlib:
	make -C libs/thatlibrary run
	cd libs/thatlibrary && cp -u thatlib.a thatlib.h ..

biglib:
	make -C libs/biglib run
	cd libs/biglib && cp -u biglib.a biglib.h ..
