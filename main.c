#include <stdio.h>
#include <stdlib.h>
#include "thatlib.h"
#include "biglib.h"

int main() {
    printf("app version 2019-02-12_2319 using library (%s [%s])\n", thatlib_version, biglib_version );
    return EXIT_SUCCESS;
}
